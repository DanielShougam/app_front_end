"""
The flask application package.
"""

from flask import Flask, url_for, render_template
app = Flask(__name__)

import FrontEnd.views
